import React from 'react'; 
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
  } from '@material-ui/pickers';



const styles = {
    formStyle: {
        display: 'flex',
        flexDirection: 'column',
        margin: 'auto',
        width: '400px',
    },
    datetimeStyle: {
        marginTop: '30px',
        width: '100%'
    },
    btnStyle:{
        width: '30%',
        margin: '50px auto',
        color: '#fff'
    }
}


function Personal (){

    const [state, setState] = React.useState({
        number: '',
         name: '',
      });
    
      const handleChange = (event) => {
        const name = event.target.name;
        setState({
          ...state,
          [name]: event.target.value,
        });
      };

      const [selectedDate, setSelectedDate] = React.useState(new Date())
      
      const handleDateChange = (date) => {
        setSelectedDate(date);
      };

      const month = (selectedDate.getMonth() + 1).toString();
      const day = selectedDate.getUTCDate().toString();
      const year = selectedDate.getUTCFullYear().toString();
      const hours = selectedDate.getHours().toString();
     

    const [value, setValue] = React.useState(false);

    const handleError =() =>{
    if(month === '9' && day === '20' && year === '2020' && hours ==='9'){
      setValue(true)
    }else{
      setValue(false)
    }
   
    }
    
 
    return(
       <div style={styles.formStyle}>
         <FormControl style={styles.datetimeStyle}>
        <InputLabel htmlFor="filled-age-native-simple">Number of Person</InputLabel>
        <Select
         native
          value={state.number}
          onChange={handleChange}
          inputProps={{
            name: 'number',
            id: 'filled-age-native-simple',
          }}
        >
          <option aria-label="None" value="" />
          <option value={1}>1 Person</option>
          <option value={2}>2 Persons</option>
          <option value={3}>3 Persons</option>
          <option value={4}>4 Persons</option>
          <option value={5}>5 Persons</option>
          <option value={6}>5+ Persons</option>
          <option value={7}>10+ Persons</option>
        </Select>
      </FormControl>

      <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <KeyboardDatePicker
          disableToolbar
          variant="inline"
          format="MM/dd/yyyy"
          margin="normal"
          id="date-picker-inline"
          label="Date"
          value={selectedDate}
          onChange={handleDateChange}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
          style={styles.datetimeStyle}
        />
        </MuiPickersUtilsProvider>
        
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardTimePicker
          margin="normal"
          id="time-picker"
          label="Time"
          value={selectedDate}
          onChange={handleDateChange}
          KeyboardButtonProps={{
            'aria-label': 'change time',
          }}
          style={styles.datetimeStyle}
        />
        </MuiPickersUtilsProvider>

        <Button variant="contained"
         color="primary" 
         style={styles.btnStyle}
         onClick={handleError}
         >
        Book Now!
      </Button>    
      <Typography style={{color: 'red'}}>
      {value === false ? " ": "The booking time is unavailable. Please try again!"}
      </Typography>
        
       </div>
    )
}

export default Personal;

