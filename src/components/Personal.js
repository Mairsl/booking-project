import React from 'react'; 
import TextField from '@material-ui/core/TextField';


const styles ={
    formStyle: {
        display: 'flex',
        flexDirection: 'column',
        margin: 'auto',
        width: '400px',
},
textfieldStyle: {
    marginTop: '30px'

}

}
function Personal (){
    return(
        <form noValidate autoComplete="off" style={styles.formStyle}>
        <TextField id="standard-basic" label="First Name" style={styles.textfieldStyle}/>
        <TextField id="standard-basic" label="Last Name" style={styles.textfieldStyle}/>
        <TextField id="standard-basic" label="Email" style={styles.textfieldStyle}/>
        <TextField id="standard-basic" label="Phone Number" style={styles.textfieldStyle}/> 
      </form>
    )
}

export default Personal;