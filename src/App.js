import React from 'react';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Personal from './components/Personal';
import Reservation from './components/Reservation';


const styles={
 tabStyle: {
margin: 'auto'
 },

 contentStyle: {
   width: '600px',
   height: '500px',
   background: '#fff',
   margin: '50px auto'
 },
 paperStyle: {
   background: '#fff',
   width: '600px',
   height: '500px',
   margin: 'auto'
 }
}


function App() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };


  return (
    <div>
      <Typography variant="h4" gutterBottom style={{textAlign: 'Center'}}>
        Book Your Table Now!
      </Typography>
      <div style={styles.paperStyle}>
    <Paper
    style={{
      margin: '50px auto',
      width: '600px',
    }}>
    <Tabs
      value={value}
       onChange={handleChange}
      indicatorColor="primary"
      textColor="primary"
      centered
    >
      <Tab 
      label="Personal Info" 
      style={styles.tabStyle}
      />

      <Tab 
      label="Reservation Info" 
      style={styles.tabStyle}
      />
    </Tabs>
  </Paper>
  
  <Paper style={styles.contentStyle}>
    {
    value === 0 ? <Personal/> : <Reservation/>
    } 
  </Paper>
  </div>
  </div>
  );
}

export default App;
